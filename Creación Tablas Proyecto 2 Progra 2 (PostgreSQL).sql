﻿CREATE DATABASE proyecto2
  WITH ENCODING='UTF8'
       OWNER=progra2
       CONNECTION LIMIT=-1;

CREATE SCHEMA ownsystems
       AUTHORIZATION progra2;

CREATE TABLE ownsystems.users
(
   id SERIAL,
   username varchar(25) NOT NULL,
   pass varchar(32) NOT NULL,
   email varchar(100),
   CONSTRAINT pk_id_users PRIMARY KEY (id)
)
WITH (
  OIDS = FALSE
)
;
ALTER TABLE ownsystems.users
  OWNER TO progra2;

CREATE TABLE ownsystems.permissions
(
   id SERIAL,
   code varchar(2) NOT NULL,
   description varchar(25) NOT NULL,
   CONSTRAINT pk_id_permissions PRIMARY KEY (id)
)
WITH (
  OIDS = FALSE
)
;
ALTER TABLE ownsystems.permissions
  OWNER TO progra2;

INSERT INTO ownsystems.permissions (code, description) VALUES (4, 'Lectura');
INSERT INTO ownsystems.permissions (code, description) VALUES (2, 'Escritura');

  CREATE TABLE ownsystems.files
(
   id SERIAL,
   filename varchar(255) NOT NULL,
   description varchar(1000) NOT NULL,
   owner_id integer not null,
   created_at date not null,
   created_by integer not null,
   updated_at date not null,
   updated_by integer not null,
   CONSTRAINT pk_id_files PRIMARY KEY (id),
   CONSTRAINT fk_owner_id_files FOREIGN KEY (owner_id) references ownsystems.users (id),
   CONSTRAINT fk_created_by_files FOREIGN KEY (created_by) references ownsystems.users (id),
   CONSTRAINT fk_updated_by_files FOREIGN KEY (updated_by) references ownsystems.users (id)
)
WITH (
  OIDS = FALSE
)
;
ALTER TABLE ownsystems.files
  OWNER TO progra2;

  CREATE TABLE ownsystems.files_users
(
   user_id integer not null,
   file_id integer NOT NULL,
   permission_id integer NOT NULL,
   CONSTRAINT pk_user_file_id PRIMARY KEY (user_id, file_id),
   CONSTRAINT fk_user_id_files_users FOREIGN KEY (user_id) references ownsystems.users (id),
   CONSTRAINT fk_file_id_files_users FOREIGN KEY (file_id) references ownsystems.files (id),
   CONSTRAINT fk_permission_id_files_users FOREIGN KEY (permission_id) references ownsystems.permissions (id)
)
WITH (
  OIDS = FALSE
)
;
ALTER TABLE ownsystems.files_users
  OWNER TO progra2;