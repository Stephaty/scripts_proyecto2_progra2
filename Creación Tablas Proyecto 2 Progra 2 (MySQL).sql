create database proyecto2;
create user 'progra2'@'localhost' identified by 'root';
grant all privileges on proyecto2.* to 'progra2'@'localhost';
flush privileges;

CREATE TABLE proyecto2.users (
  id SERIAL,
  username varchar(25) NOT NULL,
  pass varchar(32) NOT NULL,
  email varchar(100),
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE proyecto2.permissions
(
   id SERIAL,
   code varchar(2) NOT NULL,
   description varchar(25) NOT NULL,
   PRIMARY KEY (id))
ENGINE = InnoDB;

INSERT INTO proyecto2.permissions (code, description) VALUES (4, 'Lectura');
INSERT INTO proyecto2.permissions (code, description) VALUES (2, 'Escritura');

CREATE TABLE proyecto2.files
(
   id SERIAL,
   filename varchar(255) NOT NULL,
   description varchar(1000) NOT NULL,
   owner_id bigint unsigned,
   created_at date not null,
   created_by bigint unsigned,
   updated_at date not null,
   updated_by bigint unsigned,
   PRIMARY KEY (id),
   FOREIGN KEY (owner_id) references proyecto2.users (id),
   FOREIGN KEY (created_by) references proyecto2.users (id),
   FOREIGN KEY (updated_by) references proyecto2.users (id))
ENGINE = InnoDB;

CREATE TABLE proyecto2.files_users
(
   user_id bigint unsigned,
   file_id bigint unsigned,
   permission_id bigint unsigned,
   PRIMARY KEY (user_id, file_id),
   FOREIGN KEY (user_id) references proyecto2.users (id),
   FOREIGN KEY (file_id) references proyecto2.files (id),
   FOREIGN KEY (permission_id) references proyecto2.permissions (id))
ENGINE = InnoDB;